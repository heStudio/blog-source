---
title: Ubuntu for Android 内测
tag: 
    - Ubuntu
    - Linux
    - Android
    - Termux
category: 软件发布
---

::: warning 
该计划已经停止！
:::
### Ubuntu for Android 内测已经开始
#### 参与内测
你只需要在下方评论区扣一，我们会联系你的。

#### 注意事项
1. **内侧文件不要外传！！！**
2. 由于时间问题，暂未添加VNC服务，后续会添加。
3. 由于内测文件随时会改变，所以暂未将本项目安装到CDN

#### 常见问题
##### 评论系统相关
请查看以下资料

- [Talking Docs](/docs/talking/)

> 最近一次更新：2022.12.14 12:09


