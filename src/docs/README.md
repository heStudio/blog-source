---
title: 文档
icon: repo
article: false
comment: false
---

- [在Android手机上安装kali Linux 的 Q&A](/docs/kali_for_android.html)
- [heStudio BingWallpaper Get](/docs/hestudio_bing_wallpaper_get.html)
- [百度搜索提交工具](https://pypi.org/project/hbsst/)
- [Key 获取与查找](/docs/key.html)
- [补丁合集](/docs/patch.html)
- [heStudio Talking 文档](/docs/talking.html)
- [授权与备案](/docs/copyright.html)
- [Cookie 政策](/docs/cookie.html)
- [本站用到的开源服务](/docs/opensource.html)
